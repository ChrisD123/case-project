<?php

REQUIRE_ONCE('myfunctions.php'); // Include functions php file

$db = getConnection(); // Retrieve connection object and set to variable

// Start session and get session variables
session_start();
$useremail = $_SESSION['username'];
$userid = $_SESSION['user_ID'];

// If username session variable is not set, user is redirected to the Home Page
if(!isset($_SESSION['username']))
{
	header('Location: homepage.php');
}
// If admin session variable isn't set, user is redirected to Discussion Board page
else if(!isset($_SESSION['admin']))
{
	header('Location: discussionboard.php');
}
// If Submit button was pressed, the following code is executed
else if(isset($_POST['submitdiscussion']))
{
	// Stores the entered discussion title and discussion text into variables
	$disctitle = $_POST['discussiontitle'];
	$disctext = $_POST['discussiontext'];
	
	// If either fields are empty, a message is displayed
	if(empty($disctitle) || empty($disctext))
	{
		javaAlert("All fields must not be empty.");
	}
	else
	{
	// Inserts new record into the discussion table and displays message
	$discstatement = "INSERT INTO discussion (discussion_title, discussion_text, date_created, user_id) VALUES('$disctitle', '$disctext', now(), '$userid')";
	$creatediscussionquery = $db->query($discstatement);
	javaAlert("Discussion created.");
	
	}
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
 <head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title> Create Discussion </title>
<link rel="stylesheet" href="styles.css">
<h1>Create Discussion</h1>
</head>
<body>

<div class="wrapper">
    <header>
        <h1>Southumbria University</h1>
	<h3>Welcome To Your Future !</h3>
	<?php
	require_once("users.php");
	?>
	<div class="login">
            <?php
				// Displays different links depending on whether use is logged in or not
			if(logged_in()===true) {
                echo "<li><a href=\"logout.php\">Logout</a></li>";
                echo "<li><a href=\"profile.php\">My Profile</a></li>";
            }
			else{
		echo "<li><a href=\"loggingIn.php\">Login</a></li>";
                echo "<li><a href=\"register.php\">Register</a></li>";
            }
            ?>
        </div>
    </header>
    
    <nav>
        <ul>
            <li><a href="home.php">Home</a></li>
            <li class="divider">|</li>
            <li><a href="tourwelcome.php">University Tours</a></li>
            <li class="divider">|</li>
            <li><a href="discussionboard.php">Discussion Boards</a></li>
            <li class="divider">|</li>
            <li><a href="home.php">Questionnaires</a></li>
	    <li class="divider">|</li>
            <li><a href="home.php">Gallery</a></li>
        </ul>
    </nav>

<?php
// Checks whether username session variable is set
if(isset($_SESSION['username']))
{
	$username = $_SESSION['username'];
	echo "Welcome, " . $username;
	echo "<a href='logout.php'>Log Out</a>";
}
else
{
	echo "<a href='login.php'>Log In</a>";
}
?>
<a href="discussionboard.php">Discussion Board</a>
</div>

<form name="creatediscussion" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
<label for="discussiontitle">Enter Discussion Title </label>
<input type="text" name="discussiontitle" /> <br />
<label for="discussiontext">Enter Discussion Text </label> <br />
<input type="text" name="discussiontext" class="mytext" /> <br />
<input type="submit" name="submitdiscussion" value="Create Discussion" />


<footer>
		<br /><br />&copy; 2014 Southumbria Univerisity 2015. All rights reserved. <br /><br />Created by Southumbria Univeristy
	</footer>
</div>	

</body>
</html>