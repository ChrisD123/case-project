<?php

// Returns database variable
function getConnection(){
	try{

		$db = new PDO('mysql:host=localhost;dbname=unn_w11010050','unn_w11010050','abcd1234');
		return $db;
	}
	catch(PDOException $e)
	{
		echo "Connection Error: " . $e->getMessage();
	}
}

// Creates an alert message using javascript
function javaAlert($message){
	echo "<script>
alert('$message');
</script>";
}

?>