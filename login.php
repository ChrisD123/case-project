<?php

REQUIRE_ONCE('myfunctions.php'); // Include functions php file

$db = getConnection(); // Retrieve connection object and set to variable

session_start(); // Start session

// If username session variable is not set, user is redirected to the Home Page
if(isset($_SESSION['username']))
{
	header('Location: homepage.php');
}
// Checks if submit button was pressed, and the following code is executed
else if(isset($_POST['submit'])){

	// Stores e-mail that the user entered
	$useremail = trim($_POST['email']);

	$useremailquery = $db->query("SELECT * FROM user WHERE email = '$useremail'");



	while($obj = $useremailquery->fetchObject())
	{
		$queryemail = $obj->email;
		$queryid = $obj->user_id;


	}

	if(strcmp($useremail, $queryemail) == 0)
	{
		$_SESSION['username'] = $useremail;
		$_SESSION['user_ID'] = $queryid;
		$_SESSION['admin'] = "yes";
		javaAlert("Logging in...");
		header("Location: homepage.php");
	}
	else
	{
		echo "E-mails do not match.";
	}
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
 <head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title> Log In - Southumbria University </title>
<h2>Login</h2>
</head>
<body>

<form name="loginform" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
E-mail Address: <input type="text" name="email"/> <br />
Password: <input type="password" name="userpassword" /> <br />

<input type="submit" name="submit" value="Log in"/>


</form>
</div>
</div>
</body>
</html>