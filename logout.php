<?php

// Start session
session_start();

// Get user id stored as session variable
$userid = $_SESSION['user_ID'];

// If user is not logged in, user is redirected to homepage
if(!isset($_SESSION['username']))
{
	header('Location: homepage.php');
}

// If Logout button is clicked, session is destroyed and user is redirected to homepage
if(isset($_POST['logout']))
{
	session_unset();
	session_destroy();
	header("Location: homepage.php");
}

?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
 <head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title> Log Out </title>
<link rel="stylesheet" href="caseproject.css">
</head>
<body>



<form name="logoutform" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
<label for="logout">Are you sure you want to log out? </label> <br />
<input type="submit" name="logout" value="Log Out"/>
<a href="homepage.php">
   <button>Back</button>
</a>
</form>



</body>
</html>
