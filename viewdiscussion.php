<?php

REQUIRE_ONCE('myfunctions.php'); // Include functions php file

$db = getConnection(); // Retrieve connection object and set to variable

// Start session and get session variables
session_start();
$userid = $_SESSION['user_ID'];
$discussionid = $_GET['id'];

// Queries the database for the specified discussion
$discidquery = $db->query("SELECT * FROM discussion WHERE discussion_id = '$discussionid'");
$discidnumrows = $discidquery->rowCount();

// If username session variable is not set, user is redirected to the Home Page
if(!isset($_SESSION['username']))
{
	header('Location: homepage.php');
}
// If discussionid variable is empty or the query didn't return any results, redirect user to the Discussion Board page
else if(empty($discussionid) || $discidnumrows < 1)
{
	header('Location: discussionboard.php');
}

// If submit button was pressed, the following code is executed
if(isset($_POST['submitpost']))
{
	$posttext = $_POST['posttext']; // Saves entered post text to a variable
	
	// Checks if the text area for post text is empty
	if(!empty($posttext))
	{
	// Inserts a new record into the post table and alerts the user
	$createpost = $db->query("INSERT INTO post(discussion_id, user_id, post_text, date_created, flagged) VALUES('$discussionid', '$userid','$posttext', now(), '0')");
	javaAlert("Post created.");
	}
	else
	{
		// Displays a message if the post text area is empty
		javaAlert("Text box must not be empty.");
	}
	
}


?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
 <head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title> View Discussion </title>
<link rel="stylesheet" href="caseproject.css">
</head>
<body>
<a href="homepage.php">Homepage</a>
<a href="discussionboard.php">Discussion Board </a>

<?php

$discussionquery = $db->query("SELECT * FROM discussion WHERE discussion_id = '$discussionid'");

/**
 * Details for this discussion.
 **/
while($obj = $discussionquery->fetchObject())
{
	$discussiontitle = $obj->discussion_title;
	$discussiontext = $obj->discussion_text;
	$disc_datecreated = $obj->date_created;
	$disc_userid = $obj->user_id;
}

$discussionuserquery = $db->query("SELECT * FROM user WHERE user_id = '$disc_userid'");

/**
 * Details of user who created discussion.
 **/
while($obj = $discussionuserquery->fetchObject())
{
	$disc_firstname = $obj->first_name;
	$disc_surname = $obj->last_name;
}

echo "<h1> $discussiontitle </h1>";
echo "<h2>Created by: " . $disc_firstname . " " . $disc_surname;
echo "<h2>$disc_datecreated</h2>";
echo "<h3>$discussiontext </h3>";



/**
 * Get posts for this discussion.
 **/

if($postquery = $db->query("SELECT * FROM post WHERE discussion_id = '$discussionid'"))
{
	while($obj = $postquery->fetchObject())
	{
		$post_userid = $obj->user_id;
		$post_ID = $obj->post_id;
		$post_text = $obj->post_text;
		$post_created = $obj->date_created;
		
		if($userpostquery = $db->query("SELECT * FROM user WHERE user_id = '$post_userid'"))
		{
			
			while($newobj = $userpostquery->fetchObject())
			{
				$userfirstname = $newobj->first_name;
				$usersurname = $newobj->last_name;
			}	
		}

		echo $userfirstname . " " . $usersurname . "<br />" . $post_text . "<br />" . $post_created . "<br />";
		echo "<a href='flagpost.php?id=" . $post_ID . "'>Flag Post</a>" . "<br /> <br />";
	}

}

?>

<form name="postform" method="post" action="<?php echo 'viewdiscussion.php?id=' . $discussionid ?>">
<label for="posttext">Please enter text to post. </label> <br />
<input type="text" id="posttext" name="posttext" class="mytext"/> <br />
<input type="submit" name="submitpost" value="Post"/>
</form>



</body>
</html>