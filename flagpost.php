<?php
REQUIRE_ONCE('myfunctions.php'); // Include functions php file

// Start session
session_start();
$userid = $_SESSION['user_ID']; // Get ID of current user

// Retrieve connection object and set to variable
$db = getConnection();

// Get ID of the post to flag and set it to variable
$postid = $_GET['id'];

// Query database for the post using the post ID
$postidquery = $db->query("SELECT * FROM post WHERE post_id = '$postid'");
$postidnumrows = $postidquery->rowCount();

// If username session variable is not set, user is redirected to the Home Page
if(!isset($_SESSION['username']))
{
	header('Location: homepage.php');
}
// If post id session variable isn't set, user is redirected to the Discussion Board page
else if(!isset($_GET['id']))
{
	header('Location: discussionboard.php');
}
// If submit button is clicked, update database and set post as flagged
else if(isset($_POST['submitflag']))
{
$flagpost = $db->query("UPDATE post SET flagged = flagged + 1 WHERE post_id = '$postid'");
header('Location: discussionboard.php');	
}

?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Flag Post</title>
</head>

<body>
<?php
// Query database for specific post
if($postquery = $db->query("SELECT * FROM post WHERE post_id = '$postid'"))
{
	
while($obj = $postquery->fetchObject())
{
	// Get data from post table and set to variables
	$postuserid = $obj->user_id;
	$posttext = $obj->post_text;
	$postid = $obj->post_id;
	
	// Get data from user table of the user who created the post
	$userquery = $db->query("SELECT first_name, last_name FROM user WHERE user_id = '$postuserid'");
	
	while($obj = $userquery->fetchObject())
	{
		// Set data to variables
		$userfirstname = $obj->first_name;
		$usersurname = $obj->last_name;
	}
	
	// Display user details and post text
	echo $userfirstname . " " . $usersurname . "<br />" . $posttext . "<br />";
}

}
?>

<form name="flagpost" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
<label for="submitflag">Are you sure you want to flag this post? </label> <br />
<input type="submit" name="submitflag" value="Flag Post"/>
<a href="discussionboard.php">
   <button>Back</button>
</a>
</form>
</body>
</html>