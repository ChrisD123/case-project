<?php

REQUIRE_ONCE('myfunctions.php'); // Include functions php file

$db = getConnection(); // Retrieve connection object and set to variable

// Start session
session_start();

// If username session variable is not set, user is redirected to Homepage
if(!isset($_SESSION['username']))
{
	header('Location: homepage.php');
}

?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
 <head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title> Discussion Board </title>
<link rel="stylesheet" href="styles.css">
</head>
<body>

<div class="wrapper">
    <header>
        <h1>Southumbria University</h1>
	<h3>Welcome To Your Future !</h3>
	<?php
	require_once("users.php");
	?>
	<div class="login">
            <?php 
			// Checks if user is logged in, will display different links
			if(logged_in()===true) {
                echo "<li><a href=\"logout.php\">Logout</a></li>";
                echo "<li><a href=\"profile.php\">My Profile</a></li>";
            }else{
		echo "<li><a href=\"loggingIn.php\">Login</a></li>";
                echo "<li><a href=\"register.php\">Register</a></li>";
            }
            ?>
        </div>
    </header>
    
    <nav>
        <ul>
            <li><a href="home.php">Home</a></li>
            <li class="divider">|</li>
            <li><a href="tourwelcome.php">Univerisity Tours</a></li>
            <li class="divider">|</li>
            <li><a href="discussionboard.php">Discussion Boards</a></li>
            <li class="divider">|</li>
            <li><a href="home.php">Questionnaires</a></li>
	    <li class="divider">|</li>
            <li><a href="home.php">Gallery</a></li>
        </ul>
    </nav>

<h1 class='center'>Discussion Board</h1>
<?php
// Checks if user is an admin, and will display Create Discussion link if so
if(isset($_SESSION['admin']))
{
echo "<a href='creatediscussion.php'>Create Discussion</a>";
}
?>
<a href="homepage.php">Homepage</a>


<?php

// Queries database to retrieve all records from discussion table
$discussionquery = $db->query("SELECT * FROM discussion");
$discnumrows = $discussionquery->rowCount();

if($discnumrows > 0)
{

$discnumrows = $discussionquery->rowCount();

while($obj = $discussionquery->fetchObject())
{
	// Saves data from database into variables to display
	$discussionid = $obj->discussion_id;
	$discusstitle = $obj->discussion_title;
	$datecreated = $obj->date_created;
	$userid = $obj->user_id;
	
	// Queries database for details of the user who created the discussion
	if($userquery = $db->query("SELECT * FROM user WHERE user_id = '$userid'"))
	{
		
		while($newobj = $userquery->fetchObject())
		{
			$userfirstname = $newobj->first_name;
			$usersurname = $newobj->last_name;
		}
		
	}

	// Displays details obtained from database
	echo "<div class='discussion'>";
	echo "<a href='viewdiscussion.php?id=$discussionid'>$discusstitle</a>" . "<br />" . $datecreated . "<br />" . $userfirstname . " " . $usersurname;
	echo "</div>";


}


}
else
{
	echo "No records found.";
}





?>

<footer>
		<br /><br />&copy; 2014 Southumbria Univerisity 2015. All rights reserved. <br /><br />Created by Southumbria Univeristy
	</footer>
</div>

</body>
</html>

