<?php
// Start session
session_start();

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
 <head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title> Southumbria University </title>
<link rel="stylesheet" href="styles.css">
</head>
<body>
<h1>Southumbria University - Homepage</h1>

<div class="wrapper">
    <header>
        <h1>Southumbria University</h1>
	<h3>Welcome To Your Future !</h3>
	<?php
	require_once("users.php");
	?>
	<div class="login">
            <?php
			// Checks if user is logged in, and displays different links
			if(logged_in()===true) {
                echo "<li><a href=\"logout.php\">Logout</a></li>";
                echo "<li><a href=\"profile.php\">My Profile</a></li>";
            }else{
		echo "<li><a href=\"loggingIn.php\">Login</a></li>";
                echo "<li><a href=\"register.php\">Register</a></li>";
            }
            ?>
        </div>
    </header>
    
    <nav>
        <ul>
            <li><a href="home.php">Home</a></li>
            <li class="divider">|</li>
            <li><a href="tourwelcome.php">Univerisity Tours</a></li>
            <li class="divider">|</li>
            <li><a href="discussionboard.php">Discussion Boards</a></li>
            <li class="divider">|</li>
            <li><a href="home.php">Questionnaires</a></li>
	    <li class="divider">|</li>
            <li><a href="home.php">Gallery</a></li>
        </ul>
    </nav>
    
    <footer>
		<br /><br />&copy; 2014 Southumbria Univerisity 2015. All rights reserved. <br /><br />Created by Southumbria Univeristy
	</footer>
</div>	

</body>
</html>